package denys.products.memento;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import denys.products.model.Product;

/**
 * Created by denys on 26.11.17.
 */

public class ProductListMemento {
    private static ProductListMemento productListMemento;
    private static List<Product> productList = new ArrayList<>();

    private ProductListMemento() {
    }

    public static ProductListMemento getProductListMemento() {
        if (productListMemento == null)
            productListMemento = new ProductListMemento();
        return productListMemento;
    }

    public void setProductsListFromJson(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jo = jsonArray.getJSONObject(i);
            Product p = new Product();
            p.setId(jo.getInt("id"));
            p.setName(jo.getString("name"));
            p.setPrice(jo.getInt("price"));
            p.setQuontity(jo.getInt("quontity"));
            p.setBought(jo.getBoolean("bought"));
            productList.add(p);
        }
    }

    public List<Product> getProductList() {
        return productList;
    }
}
