package denys.products.memento;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

/**
 * Created by denys on 23.10.17.
 */

public class SharedPreferencesMemento {
    private static SharedPreferencesMemento memento;
    private static Context context;
    private static SharedPreferences settings;

    private SharedPreferencesMemento() {
    }

    public static SharedPreferencesMemento getMemento() {
        if(memento == null)
            memento = new SharedPreferencesMemento();
        return memento;
    }

    public static SharedPreferences getSettings() {
        return settings;
    }

    public static void setSettings(Context c) {
        settings = c.getSharedPreferences("settings", 0);
    }

    public static int getBackground(){
        return settings.getInt("background", 0);
    }

    public static void setBackground(String color){

        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("background", Color.parseColor(color));
        editor.commit();
    }

    public static int getSize(){
        return settings.getInt("size", 10);
    }

    public static void setTextSize(int size){
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("size", size);
        editor.commit();
    }
}
