package denys.products.memento;

import android.content.Intent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by denys on 23.10.17.
 */

public class IntentMemento {
    private static IntentMemento intentMemento;
    private static Map<String, Intent> memento = new HashMap<>();

    private IntentMemento() {
    }

    public static IntentMemento getIntentMemento() {
        if (intentMemento == null)
            intentMemento = new IntentMemento();
        return intentMemento;
    }

    public void addIntent(String memento, Intent intent) {
        this.memento.put(memento, intent);
    }

    public Intent getIntent(String intent) {
        Intent i = memento.get(intent);
        return i;
    }

    public List<Intent> getAll(){
        List<Intent> i = new ArrayList<>(memento.size());
        for(Map.Entry<String, Intent> map : memento.entrySet()){
            i.add(map.getValue());
        }
        return i;
    }
}
