package denys.products.model;

/**
 * Created by denys on 22.10.17.
 */
public class Product {
    private int id;
    private String name;
    private Integer price;
    private Integer quontity;
    private Boolean isBought;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getQuontity() {
        return quontity;
    }

    public void setQuontity(Integer quontity) {
        this.quontity = quontity;
    }

    public Boolean getBought() {
        return isBought;
    }

    public void setBought(Boolean bought) {
        isBought = bought;
    }
}
