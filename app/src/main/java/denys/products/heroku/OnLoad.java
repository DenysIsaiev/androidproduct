package denys.products.heroku;

import org.json.JSONException;

/**
 * Created by denys on 26.11.17.
 */

public interface OnLoad {
    void onLoadCompeted(String json) throws JSONException;
    void onLoadFailed(String error);
}
