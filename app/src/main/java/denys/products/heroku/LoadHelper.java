package denys.products.heroku;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by denys on 26.11.17.
 */

public class LoadHelper extends AsyncTask<String, Integer, String> {

    private OnLoad listener;
    private String error = "";

    public LoadHelper(OnLoad listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        StringBuffer buffer = new StringBuffer();
        try {
            URL url = new URL(params[0]);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            return buffer.toString();
        } catch (Exception e) {
            error = e.getMessage();
            return "";
        }
    }

    @Override
    protected void onPostExecute(String responce) {
        if(error.equals("")) {
            try {
                listener.onLoadCompeted(responce);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            listener.onLoadFailed(error);
        }
    }
}
