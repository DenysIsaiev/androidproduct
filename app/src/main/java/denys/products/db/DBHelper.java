package denys.products.db;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.File;

/**
 * Created by denys on 21.10.17.
 */

public class DBHelper extends SQLiteAssetHelper {
    private static final String DB_NAME = "products.db";
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }



}
