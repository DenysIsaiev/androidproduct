package denys.products.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import denys.products.model.Product;

/**
 * Created by denys on 22.10.17.
 */

public class DBAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DBAccess instance;

    private DBAccess(Context context) {
        this.openHelper = new DBHelper(context);
    }

    public static DBAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DBAccess(context);
        }
        return instance;
    }

    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public List<Product> getProducts() {
        List<Product> list = new ArrayList<>();
        Cursor cursor = database.query("products", null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Product p = new Product();
            p.setName(cursor.getString(0));
            p.setPrice(Integer.parseInt(cursor.getString(1)));
            p.setQuontity(Integer.parseInt(cursor.getString(2)));
            p.setBought(cursor.getString(3).equals("0") ? false : true );
            cursor.moveToNext();
            list.add(p);
        }
        cursor.close();
        return list;
    }

    public void addProduct(ContentValues values){
        database.insert("products", null, values);
    }

    public void deleteProduct(String where, String[] args){
        database.delete("products", where, args);
    }


}
