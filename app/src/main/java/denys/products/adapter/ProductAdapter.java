package denys.products.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import denys.products.model.Product;
import denys.products.R;

/**
 * Created by denys on 22.10.17.
 */

public class ProductAdapter extends ArrayAdapter {
    List<Product> products;
    LayoutInflater lInflater;
    Context context;

    public ProductAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.products = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rowView = inflater.inflate(R.layout.product, parent, false);
        TextView name = ((TextView) rowView.findViewById(R.id.textViewNameList));
        TextView price =((TextView) rowView.findViewById(R.id.textViewPriceList));
        TextView quontity =((TextView) rowView.findViewById(R.id.textViewQuontityList));
        CheckBox isBought =((CheckBox) rowView.findViewById(R.id.isBoughtList));

        name.setText("Name: " + products.get(position).getName());
        price.setText("Price: " + products.get(position).getPrice()+"");
        quontity.setText("Quontity: " + products.get(position).getQuontity()+"");
        isBought.setChecked(products.get(position).getBought());
        rowView.setLongClickable(true);
        return rowView;
    }
}
