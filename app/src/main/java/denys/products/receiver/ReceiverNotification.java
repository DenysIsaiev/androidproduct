package denys.products.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import denys.products.activity.ProductListActivity;


/**
 * Created by denys on 19.11.17.
 */

public class ReceiverNotification extends BroadcastReceiver{
    String packageString;
    String className;
    String notificationString;
    static int nitifiId = 101;
    Intent i = new Intent();
    @Override
    public void onReceive(Context context, Intent intent) {
        packageString = intent.getStringExtra("package");
        className = intent.getStringExtra("className");
        notificationString = intent.getStringExtra("notification");

        i.setClassName(packageString, className);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(i);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context).setContentTitle("SMB Notification").setContentText(notificationString).setSmallIcon(1);
        builder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(nitifiId++,builder.build());    }

    private class internalActivity extends AppCompatActivity{
        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            startActivity(i);
        }
    }
}
