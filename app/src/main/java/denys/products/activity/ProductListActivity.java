package denys.products.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import denys.products.adapter.ProductAdapter;
import denys.products.heroku.LoadHelper;
import denys.products.heroku.OnLoad;
import denys.products.memento.IntentMemento;
import denys.products.memento.ProductListMemento;
import denys.products.model.Product;
import denys.products.R;
import denys.products.memento.SharedPreferencesMemento;


/**
 * Created by denys on 18.10.17.
 */

public class ProductListActivity extends AppCompatActivity {
    ListView products;
    Button addProductButton;
    List<Product> productsList;
    String URL = "http://young-lowlands-47911.herokuapp.com/all";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        addProductButton = (Button) findViewById(R.id.buttonAdd);
        this.products = (ListView) findViewById(R.id.products);

        LoadHelper loaderHelper = new LoadHelper(new OnLoad() {
            @Override
            public void onLoadCompeted(String json) throws JSONException {
                Log.i("json",json);
                ProductListMemento.getProductListMemento().setProductsListFromJson(json);
            }

            @Override
            public void onLoadFailed(String error) {

            }
        });
        loaderHelper.execute(URL);

        addProductButton.getRootView().setBackgroundColor(SharedPreferencesMemento.getBackground());
        setTextSizeToElements(SharedPreferencesMemento.getSize());
    }

    @Override
    protected void onStart() {
        super.onStart();
        productsList = ProductListMemento.getProductListMemento().getProductList();
        ProductAdapter adapter = new ProductAdapter(this, android.R.layout.simple_list_item_1, productsList);
        this.products.setAdapter(adapter);
        productsList = ProductListMemento.getProductListMemento().getProductList();
        registerForContextMenu(products);
        products.getRootView().refreshDrawableState();
    }

    public void addListenersOnButtonAddProduct(View v) {
        IntentMemento.getIntentMemento().addIntent("AddProductActivity", new Intent(getApplicationContext(), AddProductActivity.class));
        startActivity(IntentMemento.getIntentMemento().getIntent("AddProductActivity"));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(1, 1, 1, "Delete");
    }

//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case 1:
//                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//                databaseAccess.open();
//                String[] s = new String[]{productsList.get(info.position).getName(),
//                        productsList.get(info.position).getPrice() + "", productsList.get(info.position).getQuontity() + ""};
//                databaseAccess.deleteProduct("Name =? AND Price =? AND Quantity =?", s);
//                databaseAccess.close();
//                reloadWrapper();
//                return true;
//            default:
//                return false;
//        }
//    }

    public void reloadWrapper() {
        Intent i = new Intent(this, this.getClass());
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivityForResult(i, 0);
        overridePendingTransition(0, 0);
    }

    private void setTextSizeToElements(int size){
        float sizeI = (float)size;
        this.addProductButton.setTextSize(sizeI);
    }

    @Override
    public void onBackPressed() {
        Intent i = IntentMemento.getIntentMemento().getIntent("MainActivity");
        finish();
        this.startActivityForResult(i, 0);
    }


}
