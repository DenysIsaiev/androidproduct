package denys.products.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import denys.products.memento.IntentMemento;
import denys.products.R;
import denys.products.memento.SharedPreferencesMemento;
import denys.products.db.DBAccess;

public class AddProductActivity extends AppCompatActivity {
    Button addProduct;
    DBAccess databaseAccess;
    ContentValues contentValues = new ContentValues();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        addProduct = (Button) findViewById(R.id.buttonAddProduct);
        databaseAccess = DBAccess.getInstance(this);
        addProduct.getRootView().setBackgroundColor(SharedPreferencesMemento.getBackground());
        setTextSizeToElements(SharedPreferencesMemento.getSize());
    }

    public void addListenersOnButtonAddProduct(View v) {
        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentValues();
                databaseAccess.open();
                databaseAccess.addProduct(contentValues);
                databaseAccess.close();

            }
        });
    }

    public void setContentValues() {
        EditText name = (EditText) findViewById(R.id.editTextName);
        EditText price = (EditText) findViewById(R.id.editTextPrice);
        EditText quantity = (EditText) findViewById(R.id.editTextQuantity);
        CheckBox isBought = (CheckBox) findViewById(R.id.checkBoxIsBought);
        contentValues.put("Name", name.getLayout().getText().toString());
        contentValues.put("Price", price.getLayout().getText().toString());
        contentValues.put("Quantity", quantity.getLayout().getText().toString());
        boolean isBoughtStr = isBought.isChecked();
        String bought = isBoughtStr ? "1" : "0";
        contentValues.put("IsBought", bought);
    }

    private void setTextSizeToElements(int size){
        float sizeI = (float)size;
        this.addProduct.setTextSize(sizeI);
        EditText editTextName = (EditText) findViewById(R.id.editTextName);
        editTextName.setTextSize(sizeI);
        EditText editTextPrice = (EditText) findViewById(R.id.editTextPrice);
        editTextPrice.setTextSize(sizeI);
        EditText editTextQuantity = (EditText) findViewById(R.id.editTextQuantity);
        editTextQuantity.setTextSize(sizeI);
        CheckBox isBought = (CheckBox) findViewById(R.id.checkBoxIsBought);
        isBought.setTextSize(sizeI);
        TextView textViewName = (TextView) findViewById(R.id.textViewName);
        textViewName.setTextSize(sizeI);
        TextView textViewPrice = (TextView) findViewById(R.id.textViewPrice);
        textViewPrice.setTextSize(sizeI);
        TextView textViewQuantity = (TextView) findViewById(R.id.textViewQuantity);
        textViewQuantity.setTextSize(sizeI);
        TextView textViewIsBought = (TextView) findViewById(R.id.textViewIsBought);
        textViewIsBought.setTextSize(sizeI);
    }

    @Override
    public void onBackPressed() {
        Intent i = IntentMemento.getIntentMemento().getIntent("ProductListActivity");
        finish();
        this.startActivityForResult(i, 0);
    }

}
