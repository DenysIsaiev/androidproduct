package denys.products.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import denys.products.memento.IntentMemento;
import denys.products.R;
import denys.products.memento.SharedPreferencesMemento;

public class MainActivity extends AppCompatActivity {
    Button buttonProductList;
    Button buttonOptions;
    Button buttonReceiveNotifications;
    Animation animRotate_button;
    Animation animRotateReverse_button;

    public void addListenersOnButtonProductList(View v) {
        buttonProductList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentMemento.getIntentMemento().addIntent("ProductListActivity", new Intent(getApplicationContext(), ProductListActivity.class));
                startActivity(IntentMemento.getIntentMemento().getIntent("ProductListActivity"));
                overridePendingTransition(R.anim.right_swipe, R.anim.right_swipe);
                finish();
            }
        });
    }

    public void addListenersOnButtonOptions(View v) {
        buttonOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentMemento.getIntentMemento().addIntent("OptionsActivity", new Intent(getApplicationContext(), OptionsActivity.class));
                startActivity(IntentMemento.getIntentMemento().getIntent("OptionsActivity"));
                overridePendingTransition(R.anim.left_swipe, R.anim.left_swipe);
                finish();
            }
        });

    }

    public void addListenersOnReceiveNotification(View v) {
                IntentMemento.getIntentMemento().addIntent("ReceiveNotificationActivity", new Intent(getApplicationContext(), ReceiveNotificationActivity.class));
                startActivity(IntentMemento.getIntentMemento().getIntent("ReceiveNotificationActivity"));
                overridePendingTransition(R.anim.right_swipe, R.anim.right_swipe);
                finish();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentMemento.getIntentMemento().addIntent("MainActivity", new Intent(getApplicationContext(), MainActivity.class));
        buttonProductList = (Button) findViewById(R.id.buttonProductList);
        buttonOptions = (Button) findViewById(R.id.buttonOptions);
        buttonReceiveNotifications = (Button) findViewById(R.id.buttonReceiveNotifications);
        animRotateReverse_button = AnimationUtils.loadAnimation(this, R.anim.rotate_inverse);
        animRotate_button = AnimationUtils.loadAnimation(this, R.anim.rotate);
        SharedPreferencesMemento.setSettings(this);
        buttonOptions.getRootView().setBackgroundColor(SharedPreferencesMemento.getBackground());
        setTextSizeToElements(SharedPreferencesMemento.getSize());
        buttonProductList.startAnimation(animRotate_button);
        buttonOptions.startAnimation(animRotateReverse_button);
        buttonReceiveNotifications.startAnimation(animRotate_button);
    }

    private void setTextSizeToElements(int size){
        float sizeI = (float)size;
        this.buttonProductList.setTextSize(sizeI);
        this.buttonOptions.setTextSize(sizeI);
        this.buttonReceiveNotifications.setTextSize(sizeI);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
