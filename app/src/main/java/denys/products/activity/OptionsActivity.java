package denys.products.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import denys.products.memento.IntentMemento;
import denys.products.R;
import denys.products.memento.SharedPreferencesMemento;

import static android.app.PendingIntent.getActivity;

/**
 * Created by denys on 18.10.17.
 */

public class OptionsActivity extends AppCompatActivity {
    Button setOptions;
    String background;
    Integer size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        setOptions = (Button) findViewById(R.id.buttonSetOptions);
        SharedPreferencesMemento.setSettings(this);
        setOptions.getRootView().setBackgroundColor(SharedPreferencesMemento.getBackground());
        setTextSizeToElements(SharedPreferencesMemento.getSize());
    }

    public void addListenersOnButtonSetOptions(View v) {
        setOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOptions();
                setOptions.getRootView().setBackgroundColor(SharedPreferencesMemento.getBackground());
                setTextSizeToElements(SharedPreferencesMemento.getSize());
                v.getRootView().refreshDrawableState();
            }
        });
    }

    public void setOptions() {
        EditText backgroundEdit = (EditText) findViewById(R.id.editTextBackground);
        EditText sizeEdit = (EditText) findViewById(R.id.editTextSize);
        this.background = backgroundEdit.getLayout().getText().toString();
        this.size = Integer.parseInt(sizeEdit.getLayout().getText().toString());
        SharedPreferencesMemento.setBackground(background);
        SharedPreferencesMemento.setTextSize(size);
    }

    private void setTextSizeToElements(int size){
        float sizeI = (float)size;
        EditText backgroundEdit = (EditText) findViewById(R.id.editTextBackground);
        backgroundEdit.setTextSize(sizeI);
        EditText sizeEdit = (EditText) findViewById(R.id.editTextSize);
        sizeEdit.setTextSize(sizeI);
        TextView backgroundText = (TextView) findViewById(R.id.textViewBackground);
        backgroundText.setTextSize(sizeI);
        TextView sizeText = (TextView) findViewById(R.id.textViewSize);
        sizeText.setTextSize(sizeI);
        this.setOptions.setTextSize(sizeI);
    }

    @Override
    public void onBackPressed() {
        Intent i = IntentMemento.getIntentMemento().getIntent("MainActivity");
        finish();
        this.startActivityForResult(i, 0);
    }
}
