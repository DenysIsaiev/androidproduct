package denys.products.activity;


import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import denys.products.R;
import denys.products.memento.IntentMemento;
import denys.products.memento.SharedPreferencesMemento;
import denys.products.receiver.ReceiverNotification;

/**
 * Created by denys on 19.11.17.
 */

public class ReceiveNotificationActivity extends AppCompatActivity {
    Button buttonReceive;
    EditText editTextNotification;
    ReceiverNotification receiverNotification = new ReceiverNotification();
    int nitifiId = 101;

    public void receiveNotification(View v) {
        Intent intent = new Intent();//getApplicationContext(), ProductListActivity.class
//        intent.setClassName("denys.products", "denys.products.activity.ProductListActivity");
        intent.putExtra("package", "denys.products");
        intent.putExtra("className", "denys.products.activity.ProductListActivity");
        intent.putExtra("notification", editTextNotification.getLayout().getText().toString());
//        intent.putExtra("a", getApplicationContext());

//        intent.setComponent(new ComponentName("denys.receiver", "com.example.messer.myapplicationb.MainActivity"));
        String permissions = Manifest.permission.ACCESS_NOTIFICATION_POLICY;

        intent.setAction("denys.products.activity.ProductListActivity");

        sendBroadcast(intent, permissions);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_notification);
        buttonReceive = (Button) findViewById(R.id.buttonReceiveNotification);
        SharedPreferencesMemento.setSettings(this);
        editTextNotification = (EditText) findViewById(R.id.editTextNotification);
        buttonReceive.getRootView().setBackgroundColor(SharedPreferencesMemento.getBackground());
        setTextSizeToElements(SharedPreferencesMemento.getSize());

    }

    private void setTextSizeToElements(int size){
        float sizeI = (float)size;
        this.buttonReceive.setTextSize(sizeI);
    }

    @Override
    public void onBackPressed() {
    finish();
}

}
