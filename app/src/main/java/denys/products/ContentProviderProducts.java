package denys.products;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import denys.products.db.DBAccess;
import denys.products.db.DBHelper;

/**
 * Created by denys on 24.10.17.
 */

public class ContentProviderProducts extends ContentProvider {

    static final String PRODUCT_TABLE = "products";
    private static final UriMatcher uriMatcher;
    static final String PRODUCT_ID = "_id";
    static final String AUTHORITY = "denys.products";
    static final String PRODUCT_PATH = "products";
    static final int URI_PRODUCTS = 1;
    static final int URI_PRODUCTS_ID = 2;
    static final String PRODUCT_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + PRODUCT_PATH;

    static final String PRODUCT_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + PRODUCT_PATH;

    public static final Uri PRODUCT_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + PRODUCT_PATH);

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, PRODUCT_PATH, URI_PRODUCTS);
        uriMatcher.addURI(AUTHORITY, PRODUCT_PATH + "/#", URI_PRODUCTS_ID);
    }

    private SQLiteDatabase database ;
    private DBHelper dbHelper;

    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        database = dbHelper.getWritableDatabase();
        long rowID = database.insert(PRODUCT_TABLE, null, values);
        Uri resultUri = ContentUris.withAppendedId(PRODUCT_CONTENT_URI, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        switch (uriMatcher.match(uri)){
            case URI_PRODUCTS:
                break;
            case URI_PRODUCTS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = PRODUCT_ID + " = " + id;
                } else {
                    selection = selection + " AND " + PRODUCT_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        database = dbHelper.getWritableDatabase();
        int cnt = database.delete(PRODUCT_TABLE, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        switch (uriMatcher.match(uri)) {
            case URI_PRODUCTS:
                break;
            case URI_PRODUCTS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = PRODUCT_ID + " = " + id;
                } else {
                    selection = selection + " AND " + PRODUCT_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        database = dbHelper.getWritableDatabase();
        int cnt = database.update(PRODUCT_TABLE, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case URI_PRODUCTS:
                return PRODUCT_CONTENT_TYPE;
            case URI_PRODUCTS_ID:
                return PRODUCT_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        switch (uriMatcher.match(uri)) {
            case URI_PRODUCTS:
                break;
            case URI_PRODUCTS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = PRODUCT_ID + " = " + id;
                } else {
                    selection = selection + " AND " + PRODUCT_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query("products", projection, selection,
                selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(),
                PRODUCT_CONTENT_URI);
        return cursor;
    }
}
